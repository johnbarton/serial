#include <unistd.h>
#include <exception>
#include <iostream>
#include <string>
#include <memory>

#if defined(__linux__)
#include <pty.h>
#else
#include <util.h>
#endif

#include "serial/serial.h"

using namespace serial;
using std::string;

class TestStructure {
 public:
  Serial *port1;
  int master_fd;
  int slave_fd;
  char name[100];

  TestStructure() {
    if (openpty(&master_fd, &slave_fd, name, NULL, NULL) == -1 ||
        master_fd <= 0 || slave_fd <= 0 || string(name).length() <= 0) {
      throw std::ios_base::failure("Error opening psuedo-teletype");
    }

    port1 = new Serial(string(name), 115200, Timeout::simpleTimeout(250));
  }

  ~TestStructure() {
    port1->close();
    delete port1;
  }
};

void testRead(TestStructure *testStructure) {
  write(testStructure->master_fd, "abc\n", 4);
  string r = testStructure->port1->read(4);
  if (r.compare("abc\n") != 0) {
    throw std::ios_base::failure("Read error");
  }
}

void testWrite(TestStructure *testStructure) {
  char buf[5] = "";
  testStructure->port1->write("abc\n");
  read(testStructure->master_fd, buf, 4);
  if (string(buf, 4).compare("abc\n") != 0) {
    throw std::ios_base::failure("Write error");
  }
}

void testTimeout(TestStructure *testStructure) {
  // Timeout a read, returns an empty string
  string empty = testStructure->port1->read();
  if (!empty.empty()) {
    throw std::ios_base::failure("Timeout error");
  }

  // Ensure that writing/reading still works after a timeout.
  write(testStructure->master_fd, "abc\n", 4);
  string r = testStructure->port1->read(4);
  if (r.compare("abc\n") != 0) {
  }
}

void testPartialRead(TestStructure *testStructure) {
  // Write some data, but request more than was written.
  write(testStructure->master_fd, "abc\n", 4);

  // Should timeout, but return what was in the buffer.
  string empty = testStructure->port1->read(10);
  if (empty.compare("abc\n") != 0) {
    throw std::ios_base::failure("Partial read error");
  }

  // Ensure that writing/reading still works after a timeout.
  write(testStructure->master_fd, "abc\n", 4);
  string r = testStructure->port1->read(4);
  if (r.compare("abc\n") != 0) {
    throw std::ios_base::failure("Partial read error");
  }
}

int main(int argc, char **argv) {
  try {
    auto testStructure = new TestStructure();
    testRead(testStructure);
    testWrite(testStructure);
    testTimeout(testStructure);
    testPartialRead(testStructure);
  } catch (std::exception &e) {
    std::cerr << "Unhandled Exception: " << e.what() << std::endl;
    return -1;
  }
  
  return 0;
}
