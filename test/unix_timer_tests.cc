#include <unistd.h>
#include <exception>
#include <iostream>
#include <memory>
#include <string>

#include "serial/impl/unix.h"

using serial::MillisecondTimer;

/**
 * Do 100 trials of timing gaps between 0 and 19 milliseconds.
 * Expect accuracy within one millisecond.
 */
void testShortIntervals() {
  for (int trial = 0; trial < 100; trial++) {
    uint32_t ms = std::rand() % 20;
    MillisecondTimer mt(ms);
    usleep(1000 * ms);
    int32_t r = mt.remaining();

    // 1ms slush, for the cost of calling usleep.
    if (std::abs(r + 1 - 0) > 1) {
      throw std::runtime_error("Short interval error");
    }
  }
}

void testOverlappingLongIntervals() {
  MillisecondTimer *timers[10];

  // Experimentally determined. Corresponds to the extra time taken by the
  // loops, the big usleep, and the test infrastructure itself.
  const int slush_factor = 14;

  // Set up the timers to each time one second, 1ms apart.
  for (int t = 0; t < 10; t++) {
    timers[t] = new MillisecondTimer(1000);
    usleep(1000);
  }

  // Check in on them after 500ms.
  usleep(500000);
  for (int t = 0; t < 10; t++) {
    if (std::abs(timers[t]->remaining() - (500 - slush_factor + t) > 5)) {
      throw std::runtime_error("Overlapping long interval error");
    }
  }

  // Check in on them again after another 500ms and free them.
  usleep(500000);
  for (int t = 0; t < 10; t++) {
    if (std::abs(timers[t]->remaining() - (-slush_factor + t) > 5)) {
      throw std::runtime_error("Overlapping long interval error");
    }
    delete timers[t];
  }
}

int main(int argc, char **argv) {
  try {
    testShortIntervals();
    testOverlappingLongIntervals();
  } catch (std::exception &e) {
    std::cerr << "Unhandled Exception: " << e.what() << std::endl;
    return -1;
  }

  return 0;
}
