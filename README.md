# Serial Communication Library

This is a cross-platform library for interfacing with rs-232 serial like ports written in C++. It provides a modern C++ interface with a workflow designed to look and feel like PySerial, but with the speed and control provided by C++.

Serial is a class that provides the basic interface common to serial libraries (open, close, read, write, etc..) and requires no extra dependencies. It also provides tight control over timeouts and control over handshaking lines.

### Documentation

Website: http://wjwwood.github.com/serial/

API Documentation: http://wjwwood.github.com/serial/doc/1.1.0/index.html

To build documentation using Doxygen:
```shell
doxygen doc/Doxyfile
open doc/html/index.html
```

### Dependencies

Required:
* [cmake](http://www.cmake.org) - buildsystem

Optional (for tests): 
* [Boost](http://www.boost.org/) - Boost C++ librairies

Optional (for documentation):
* [Doxygen](http://www.doxygen.org/) - Documentation generation tool
* [graphviz](http://www.graphviz.org/) - Graph visualization software

### Install

#### OSX Using Homebrew

```
brew tap johnbarton/tap https://gitlab.com/johnbarton/homebrew-tap
brew install serial
```

#### OSX, Linux, or Windows

```
git clone https://gitlab.com/johnbarton/serial.git
cd serial
mkdir build && cd build
cmake ..
make
make install
```

### Authors

William Woodall <wjwwood@gmail.com>
John Harrison <ash.gti@gmail.com>

### Contact

For ROS/Catkin concerns or questions, contact:

William Woodall <william@osrfoundation.org>

For non-ROS/Catkin concerns, such as building and installing this library outside of ROS, contact:

John Barton <johnnybarton411@gmail.com>
